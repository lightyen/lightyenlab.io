---
title: "測試貼文"
date: 2018-12-07T15:13:46+08:00
draft: true
tags: ["test"]
categories: [
    "development",
    "golang"
]
---

### Test
`a`**b**c

---

### go 語法高亮測試
code區塊字型預設為Fira Code
```go

type Hello struct {
    name  string
    count int64
}

func (h *Hello) run (c *client) error {
    ch := make(chan int, 0)
    // ...
    <-ch
}
```


### markdown 表格

| Character | Hex  | Character | Hex  | Character | Hex  |
| :-------: | :--- | :-------: | :--- | :-------: | :--- |
| Space     | 0x20 | @         | 0x40 | `         | 0x60 |
| !         | 0x21 | A         | 0x41 | a         | 0x61 |
| "         | 0x22 | B         | 0x42 | b         | 0x62 |
| #         | 0x23 | C         | 0x43 | c         | 0x63 |
| $         | 0x24 | D         | 0x44 | d         | 0x64 |
| %         | 0x25 | E         | 0x45 | e         | 0x65 |
| &         | 0x26 | F         | 0x46 | f         | 0x66 |
| '         | 0x27 | G         | 0x47 | g         | 0x67 |
| (         | 0x28 | H         | 0x48 | h         | 0x68 |
| )         | 0x29 | I         | 0x49 | i         | 0x69 |
| *         | 0x2A | J         | 0x4A | j         | 0x6A |
| +         | 0x2B | K         | 0x4B | k         | 0x6B |
| ,         | 0x2C | L         | 0x4C | l         | 0x6C |
| -         | 0x2D | M         | 0x4D | m         | 0x6D |
| .         | 0x2E | N         | 0x4E | n         | 0x6E |
| /         | 0x2F | O         | 0x4F | o         | 0x6F |
| 0         | 0x30 | P         | 0x50 | p         | 0x70 |
| 1         | 0x31 | Q         | 0x51 | q         | 0x71 |
| 2         | 0x32 | R         | 0x52 | r         | 0x72 |
| 3         | 0x33 | S         | 0x53 | s         | 0x73 |
| 4         | 0x34 | T         | 0x54 | t         | 0x74 |
| 5         | 0x35 | U         | 0x55 | u         | 0x75 |
| 6         | 0x36 | V         | 0x56 | v         | 0x76 |
| 7         | 0x37 | W         | 0x57 | w         | 0x77 |
| 8         | 0x38 | X         | 0x58 | x         | 0x78 |
| 9         | 0x39 | Y         | 0x59 | y         | 0x79 |
| :         | 0x3A | Z         | 0x5A | z         | 0x7A |
| ;         | 0x3B | [         | 0x5B | {         | 0x7B |
| <         | 0x3C | \\        | 0x5C | \|        | 0x7C |
| =         | 0x3D | ]         | 0x5D | }         | 0x7D |
| >         | 0x3E | ^         | 0x5E | ~         | 0x7E |
| ?         | 0x3F | _         | 0x5F | DEL       | 0x7F |
