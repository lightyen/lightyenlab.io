---
title: "使用 hugo"
date: 2018-12-11T10:19:08+08:00
draft: true
tags: ["hugo"]
categories: [ "Development" ]
---

## Hugo
[Hugo](https://github.com/gohugoio/hugo) 為簡易的靜態網站建構框架，使用golang開發，只要鍵入一兩個指令就能輕鬆的建置靜態網站，也能使用markdown語法來撰寫文章。

### 安裝 hugo
- 使用的作業環境：Linux Mint
- 前往 https://github.com/gohugoio/hugo/releases
- 選擇**hugo_extended_版本號_Linux-64bit.deb**安裝  

##### 也可以下載源代碼來編譯
```shell
mkdir $HOME/src
cd $HOME/src
git clone https://github.com/gohugoio/hugo.git
cd hugo
go install --tags extended

```
> 注：extended版本支持sass/scss樣式

### 建立 hugo

```shell
# 檢查版本
hugo version

# 初始化網站
hugo new site <site_name>

# 建立貼文
hugo new posts/<filename>.md

# 測試
hugo server -p 8080 -D
```

### 建置網站
```shell
# 輸出靜態網頁
hugo

# 帶上post content
hugo -D
```

---

> 其他靜態網站生成框架參考
> https://www.staticgen.com/